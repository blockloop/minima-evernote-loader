/* eslint new-cap: [0] */

/**
 * https://dev.evernote.com/doc/reference/NoteStore.html#Struct_NoteFilter
 * https://dev.evernote.com/doc/reference/NoteStore.html#Fn_NoteStore_findNotesMetadata
 */
var slug = require('slug');
var Evernote = require('evernote').Evernote;
var enml = require('enml-js');
var moment = require('moment');
var markdown = require('markdown').markdown;
var htmlToText = require('html-to-text');
var Q = require('q');


module.exports = function(logger, config) {
    var cache = {};
    var client = new Evernote.Client({token: config.authToken, sandbox: config.sandbox});
    var noteStore = client.getNoteStore();

    /**
     * @access public
     * @callback cb - called with each note received
     */
    this.listPages = function (cb) {
        getNotebookId(config.notebookName).then(function(notebookId){
            logger.trace('getting notes for %s', config.notebookName);
            getNotes(notebookId, function(note) {
                cb({
                    identifier: note.guid,
                    title: note.title,
                    slug: slug(note.title.toLowerCase()),
                    createDate: moment.utc(note.created).toDate(),
                    modifiedDate: moment.utc(note.updated).toDate(),
                    tags: note.tags || [],
                    unpublished: note.unpublished
                });
            });
        });
    };

    /**
     * @param {Note} - a note to fetch content for
     * @returns {promise}
     */
    this.getPageContent = function(note) {
        var def = Q.defer();

        logger.trace('fetching note content for %s: "%s"', note.identifier, note.title);

        noteStore.getNote(note.identifier, true, true, true, true, function(err, found){
            var content;

            if (err) {
                logger.error(err);
                def.reject(err);
            } else if (note.tags.indexOf('markdown') === -1) {
                content = enml.HTMLOfENML(found.content, found.resources);
                note.content = content.match(/<body[^>]+>(.+)<\/body>/)[1];
            } else {
                logger.trace('converting %s to markdown', note.slug);
                content = enml.HTMLOfENML(found.content, found.resources);
                var plain = htmlToText.fromString(content, { wordwrap: false });
                note.content = markdown.toHTML(plain, 'Maruku');
            }

            logger.trace('note content for %s received', note.title);

            def.resolve(note);
        });
        return def.promise;
    };



    /****************************************
     *
     * PRIVATE FUNCTIONS
     *
     ***************************************/

    /**
     * getNotebookId
     *
     * @param {String} notebookName - the name of the notebook to lookup
     * @return {promise}
     */
    function getNotebookId(notebookName) {
        var def = Q.defer();
        if (cache.notebookGuid) {
            def.resolve(cache.notebookGuid);
            return def.promise;
        }

        logger.trace('Fetching notebook ID for ' + notebookName);
        noteStore.listNotebooks(function (err, notebooks){
            if (err) { return logger.error(err); }
            var found = notebooks.filter(function(item){
                return item.name.toLowerCase() === notebookName;
            });

            if (found && found.length === 1) {
                logger.trace('found notebook ID %s for %s', found[0].guid, notebookName);
                cache.notebookGuid = found[0].guid;
                def.resolve(cache.notebookGuid);
            } else {
                logger.trace('could not find notebook ID for %s', notebookName);
                cache.notebookGuid = null;
            }
        });
        return def.promise;
    }


    /**
     * getNotes
     *
     * @param {String} notebookId - notebookGuid from Evernote
     * @callback cb - a callback for individual notes
     */
    function getNotes(notebookId, cb) {
        var filter = new Evernote.NoteFilter();
        //set the notebook guid filter to the GUID of the default notebook
        filter.notebookGuid = notebookId;

        //create a new result spec for findNotesMetadata
        var resultSpec = new Evernote.NotesMetadataResultSpec();
        resultSpec.includeTitle = true;
        resultSpec.includeContentLength = true;
        resultSpec.includeCreated = true;
        resultSpec.includeUpdated = true;
        resultSpec.includeNotebookGuid = true;
        resultSpec.includeTagGuids = true;
        resultSpec.includeAttributes = true;

        //call findNotesMetadata on the note store
        noteStore.findNotesMetadata(filter, 0, 100, resultSpec, function(err, notesMeta) {
            if (err) { throw err; }
            else {
                //log the number of notes found in the default notebook
                logger.trace('Found %s notes', notesMeta.notes.length);
                notesMeta.notes.forEach(function(note) {
                    getNoteTags(note).then(function(tags) {
                        note.tags = tags;
                        if (note.tags.indexOf('publish') === -1) {
                            note.unpublished = true;
                        }
                        cb(note);
                    });
                });
            }});
    }

    /**
     * getNoteTags
     *
     * @param {Note} note - a note to get tags for
     * @return {promise}
     */
    function getNoteTags(note) {
        var def = Q.defer();
        logger.trace('Getting tags for %s', note.title);

        if (!note.tagGuids || note.tagGuids.length <= 0) {
            logger.trace('no tags for %s', note.title);
            def.resolve([]);
            return def.promise;
        }

        var all = note.tagGuids.map(function(tagGuid) {
            return getTag(tagGuid);
        });

        Q.allSettled(all).spread(function() {
            var tags = Array.prototype.slice.call(arguments).map(function(promise) {
                return promise.value;
            });
            logger.trace('TAGS for %s: %s', note.title, tags);
            def.resolve(tags);
        }).done();

        return def.promise;
    }

    /**
     * getTag
     *
     * @param {String} tagGuid - Tag GUID from Evernote to lookup
     * @return {undefined}
     */
    function getTag(tagGuid) {
        var def = Q.defer();
        if (cache.tags == null) {
            cache.tags = {};
        }

        logger.trace('Getting tag for tagGuid: %s', tagGuid);

        var cached = cache.tags[tagGuid];

        if (cached) {
            logger.trace('tagGuid already cached: %s', tagGuid);
            def.resolve(cached);
        } else {
            noteStore.getTag(tagGuid, function(err, tag) {
                if (err) {
                    logger.error(err);
                    def.reject(err);
                }
                cache.tags[tagGuid] = tag.name;
                def.resolve(tag.name);
            });
        }

        return def.promise;
    }


};
